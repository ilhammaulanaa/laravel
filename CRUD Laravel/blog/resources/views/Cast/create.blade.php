@extends('adminlte.master')

@section('content')
<div class="m-3">
  <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Create New Cast</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="/cast" method="POST">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama', '')}}" placeholder="Masukkan nama" required>
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" id="umur" name="umur" value="{{old('umur', '')}}" placeholder="umur" required>
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="Bio" name="bio" value="{{old('bio', '')}}" placeholder="Bio" required>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
        <!-- /.card-body -->
  
        <div class="card-footer">
          <button type="submit" value="submit" class="btn btn-primary">Create</button>
        </div>
      </form>
  </div>
</div>
@endsection