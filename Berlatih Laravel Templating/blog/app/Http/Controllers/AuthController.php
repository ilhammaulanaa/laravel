<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }

    public function welcome(Request $request){
        $nama = $request["firstname"]." ".$request["lastname"];
        // $last = $request["lastname"];
        // return view('welcome', compact('first','last'));
        return view('welcome', compact('nama'));
    }
}
